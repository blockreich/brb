package net.blockreich.blockreichbank;

import org.bukkit.Bukkit;

import org.bukkit.Location;

public class LocationWrapper {

    private String world;
    private double x;
    private double y;
    private double z;
    private float yaw;
    private float pitch;


    LocationWrapper(Location loc){
        world = loc.getWorld().getName();
        x = loc.getX();
        y = loc.getY();
        z = loc.getZ();
        yaw = loc.getYaw();
        pitch = loc.getPitch();
    }

    public Location toLocation() {
        return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
    }

}
