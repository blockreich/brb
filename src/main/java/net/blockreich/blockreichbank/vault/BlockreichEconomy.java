package net.blockreich.blockreichbank.vault;

import org.bukkit.plugin.Plugin;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BlockreichEconomy {
    private Plugin plugin;
    private Connection conn;

    BlockreichEconomy(Plugin mainPlugin) {
        plugin = mainPlugin;
        plugin.getLogger().info("BlockreichBank - Economy wird geladen...");
        connectDatabase();
        createDatabaseStructure();

    }

    private void createDatabaseStructure() {
        try {
            conn.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS `accounts` (\n" +
                    "\t`player` VARCHAR(100) NOT NULL,\n" +
                    "\t`creationDate` TIMESTAMP NOT NULL,\n" +
                    "\t`balance` DOUBLE NOT NULL,\n" +
                    "\tPRIMARY KEY (`player`)\n" +
                    ");");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void connectDatabase() {
        try {
            // db parameters
            String url = "jdbc:sqlite:" + new File(plugin.getDataFolder(), "database").getAbsolutePath();
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            plugin.getLogger().info("Datenbankverbindung aufgebaut.");

        } catch (SQLException e) {
            plugin.getLogger().info(e.getMessage());
            plugin.getLogger().severe("Datenbankfehler, deaktivere....");
            plugin.getPluginLoader().disablePlugin(plugin); //System ist ohne Datenbank nicht funktionsfähig
        }
    }
}
