package net.blockreich.blockreichbank;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.Plugin;

import javax.annotation.processing.Filer;
import java.io.*;
import java.lang.reflect.Type;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class ClickLessTriggers implements Listener {
    private HashMap<Location,String> triggers;
    private Plugin mainPlugin;
    private TriggerAction action;
    private Gson gson = new Gson();


    ClickLessTriggers(Plugin plugin, TriggerAction onTriggered) {
        mainPlugin = plugin;
        Bukkit.getPluginManager().registerEvents(this, mainPlugin);
        triggers = new HashMap<>();
        action = onTriggered;
    }


    /**
     * Adds a Trigger
     * @param location The Locaton of the Block where the Action should be triggered
     * @param action TriggerAction which will be triggered
     */
    public void addPos(Location location, String action) {
        triggers.put(location, action);
        mainPlugin.getLogger().info("New Trigger has been registered at " + location.toString());
    }
    public Set<Location> getTriggerLocations(){
        return triggers.keySet();
    }

    int moveeventcounter = 0;

    @EventHandler
    public void onMove(PlayerMoveEvent moveEvent) {
        if (!moveEvent.getFrom().getBlock().getLocation().equals(moveEvent.getTo().getBlock().getLocation())){
            mainPlugin.getLogger().info("Moved not on same block!");
            if(triggers.containsKey(moveEvent.getTo().getBlock().getLocation())){
                mainPlugin.getLogger().info("On Triggerblock!");
                action.onActionTriggered(moveEvent.getFrom().getBlock().getLocation(), moveEvent.getPlayer(),
                        triggers.get(moveEvent.getFrom().getBlock().getLocation()));
                moveEvent.setCancelled(true);
            }
        }
        moveeventcounter++;
        if (moveeventcounter > 10) {
            mainPlugin.getLogger().info("10 moveevents fired");
            moveeventcounter = 0;
        }
    }

    public void saveToFile(File file) {

        try {
            final Writer w = new FileWriter(file);
            triggers.forEach((loc, action) -> {
                try {
                    w.write(loc.getWorld() + "~" + loc.getX() + "~" + loc.getY() + "~" + loc.getZ() + "~" + loc.getYaw()
                            + "~" +  loc.getPitch() + "~" + action + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            w.close();
        } catch (IOException e) {
           //File konnte nicht geöffnet werden
        }
    }
    public void loadFromFile(File file) throws IOException, ClassNotFoundException {
        Path p = FileSystems.getDefault().getPath(file.getAbsolutePath());
        List<String> lines = Files.readAllLines(p);
        triggers.clear();
        lines.forEach((line) -> {
            String[] splits = line.split("~");
            Location loc = new Location(Bukkit.getWorld(splits[0]), Float.parseFloat(splits[1]), Float.parseFloat(splits[2])
            ,Float.parseFloat(splits[3]), Float.parseFloat(splits[4]), Float.parseFloat(splits[5]));
            triggers.put(loc, splits[6]);
        });
    }


}
