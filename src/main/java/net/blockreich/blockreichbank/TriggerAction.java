package net.blockreich.blockreichbank;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public interface TriggerAction {
    void onActionTriggered(Location location, Player victim, String action);
}
