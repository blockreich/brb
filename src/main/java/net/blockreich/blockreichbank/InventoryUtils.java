package net.blockreich.blockreichbank;

import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.HashMap;

public class InventoryUtils {
    static boolean giveStackedEmeralds(int amount, Player p) {
        if (amount <= Material.EMERALD.getMaxStackSize()) {
            if (computeFreeSlots(p.getInventory()) < 1) {
                return false;
            }
            PlayerInventory inv = p.getInventory();
            inv.addItem(new ItemStack(Material.EMERALD, amount));
            p.updateInventory();
            return true;
        } else {
            if (computeFreeSlots(p.getInventory()) > ((amount / 9) + 1)) {
                PlayerInventory inv = p.getInventory();
                inv.addItem(new ItemStack(Material.EMERALD_BLOCK, amount / 9));
                inv.addItem(new ItemStack(Material.EMERALD, amount % 9));
                p.updateInventory();
                return true;
            } else return false;
        }
    }

    private static int computeFreeSlots(PlayerInventory p) {
        ItemStack[] contents = p.getContents();
        int count = 0;
        for (ItemStack stack : contents) {
            if (stack == null)
                count++;
        }
        return count;
    }

    public static int removeDrugs(Player player, Material material) {
        PlayerInventory inventar = player.getInventory();
        ItemStack[] items = inventar.getContents();

        int anzahl = 0;
        for (int index = 0; index < items.length; index++) {
            ItemStack item = items[index];
            if (item != null && item.getType() == material) {
                anzahl += item.getAmount();
                items[index] = null;
            }
        }

        inventar.setContents(items);
        return anzahl;
    }
}
