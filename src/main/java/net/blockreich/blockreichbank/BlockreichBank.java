package net.blockreich.blockreichbank;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerCommandSendEvent;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public final class BlockreichBank extends JavaPlugin implements CommandExecutor {

    public static final String ONLYNUMBERS = ChatColor.RED + "Bitte nutze Zahlen!";
    private static final String TOLESSMONEY = ChatColor.RED + "Du hast zu wenig Geld!";
    private static final String NOTENOUGHFREESLOTS = ChatColor.RED + "Du hast nicht genügend Inventarplätze verfügbar.";
    private static final String UNKNOWERROR = ChatColor.RED + "Es ist ein unbekannter Fehler aufgetreten. Wende dich an einen Administrator.";
    private static final String TRANSACTIONSUCCESS = ChatColor.DARK_GREEN + "Die Transaktion war erfolgreich!";
    private static Economy econ = null;
    private ClickLessTriggers triggers = null;
    private ArrayList<Player> openedInterface = new ArrayList<>();
    private File datafolder;

    @Override
    public void onEnable() {
        if(!setupEconomy()) {
            getLogger().info("Fehlendes Economy-Plugin. deaktiviere BRB...");
            getPluginLoader().disablePlugin(this);
        }
        datafolder = getDataFolder();
        datafolder.mkdirs();
        getLogger().info("BlockreichBank wurde erfolgreich geladen!");

        triggers = new ClickLessTriggers(this, (location, player, action) -> {
            player.spawnParticle(Particle.VILLAGER_HAPPY, player.getLocation().add(0,1,0), 100);
            openedInterface.add(player);
            TextComponent payout = new TextComponent("Abheben");
            payout.setColor(net.md_5.bungee.api.ChatColor.DARK_GREEN);
            payout.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/auszahlen [Betrag]"));
            TextComponent payin = new TextComponent("Einzahlen");
            payin.setColor(net.md_5.bungee.api.ChatColor.DARK_GREEN);
            payin.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/einzahlen [Betrag]"));
            TextComponent toPlayer = new TextComponent("Überweisen");
            toPlayer.setColor(net.md_5.bungee.api.ChatColor.DARK_GREEN);
            toPlayer.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/transaktion [Spieler] [Betrag]"));

            player.sendMessage(ChatColor.GREEN + "-------| Blockreich Bank |-------");
            player.sendMessage(ChatColor.RED +   "                Bankomat ");
            player.sendMessage(ChatColor.GREEN + "Guthaben: " + econ.format(econ.getBalance(player)));
            player.spigot().sendMessage(payout);
            player.spigot().sendMessage(payin);
            player.spigot().sendMessage(toPlayer);
            player.sendMessage(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC + "Besuche unsere Bank für erweiterte Funktionen.");
        });

        try {
            triggers.loadFromFile(new File(datafolder, "triggers.ser"));
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            getLogger().severe("DateiIOFehler.");
        }
        //triggers.addPos(new Location(Bukkit.getWorld("world"),183,67,-65), null);


    }

    @Override
    public void onDisable() {
        triggers.saveToFile(new File(datafolder, "triggers.ser"));
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Nur Spieler können dieses Plugin nutzen.");
            return true;
        }
        Player p = (Player) sender;
        if (command.getName().equals("addbankomat")){
            if (args.length == 0) {
                Location loc = p.getLocation().getBlock().getLocation().clone();
                p.teleport(loc.clone().add(3,0,3));
                triggers.addPos(loc, "bankomat");
                p.sendMessage(ChatColor.DARK_GREEN + "Ein neuer Bankomat wurde erstellt.");
                return true;
            }
        }
        else if (command.getName().equals("listbankomats")){
            p.sendMessage(ChatColor.GREEN + "Liste Bankomaten auf...");
            AtomicInteger bankomats = new AtomicInteger();
            triggers.getTriggerLocations().forEach(location -> {
                bankomats.getAndIncrement();
                TextComponent bankomat = new TextComponent(
                        location.getWorld().getName() + " (x: "+ location.getBlockX() + "; z: " + location.getBlockZ() +")");
                bankomat.setColor(net.md_5.bungee.api.ChatColor.YELLOW);
                bankomat.setItalic(true);
                bankomat.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, ""));
                p.spigot().sendMessage(bankomat);
            });
            p.sendMessage(ChatColor.DARK_GREEN + "Es wurde(n) " + bankomats.get() + " Bankomat(en) gefunden.");
            return true;
        } else if (command.getName().equals("auszahlen")) {
            if(!openedInterface.contains(p)) {
                return true;
            }
            if(!Util.isNumber(args[0])) {
                p.sendMessage(ONLYNUMBERS);
                return true;
            }
            final int amount = Integer.parseInt(args[0]);
            if(!econ.has(p, amount)) {
                p.sendMessage(TOLESSMONEY);
                return true;
            }
            if(!InventoryUtils.giveStackedEmeralds(amount, p)) {
                p.sendMessage(NOTENOUGHFREESLOTS);
                return true;
            }
            final EconomyResponse economyResponse = econ.withdrawPlayer(p, amount);
            if(!economyResponse.transactionSuccess()){
                p.sendMessage(UNKNOWERROR);
                getLogger().warning("Error: " + economyResponse.errorMessage);
                return  true;
            }
            p.sendMessage(TRANSACTIONSUCCESS);
            openedInterface.remove(p);
        } else if (command.getName().equals("einzahlen")) {
            if(!openedInterface.contains(p)) {
                return true;
            }
            if(!Util.isNumber(args[0])) {
                p.sendMessage(ONLYNUMBERS);
                return true;
            }
            final int amount = Integer.parseInt(args[0]);
            int countEmerald = InventoryUtils.removeDrugs(p, Material.EMERALD);
            int countEmeraldBlock = InventoryUtils.removeDrugs(p, Material.EMERALD_BLOCK);

            if((countEmerald + countEmeraldBlock*9) > amount) {
                InventoryUtils.giveStackedEmeralds(((countEmerald + countEmeraldBlock*9) - amount), p);
                econ.depositPlayer(p, amount);
            }
            else {econ.depositPlayer(p,countEmerald + countEmeraldBlock);}

        } else if (command.getName().equals("transaktion")) {
            if(!openedInterface.contains(p)) {
                return true;
            }
            if(!Util.isNumber(args[1])) {
                p.sendMessage(ONLYNUMBERS);
                return true;
            }

        } else if (command.getName().equals("saveandreload")) {
            p.sendMessage("neu laden der Bankomaten..");
            try {
                triggers.saveToFile(new File(datafolder, "triggers.ser"));
                triggers.loadFromFile(new File(datafolder, "triggers.ser"));
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    @EventHandler
    public void onPlayerTab(PlayerCommandSendEvent e) {
        List<String> blockedCommands = new ArrayList<>();
        blockedCommands.add("payin");
        blockedCommands.add("payout");
        e.getCommands().removeAll(blockedCommands);
    }


}
