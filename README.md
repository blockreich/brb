# Blockreich-Bank
[![pipeline status](https://gitlab.com/blockreich/brb/badges/master/pipeline.svg)](https://gitlab.com/blockreich/brb/commits/master)
Bankplugin für den **Blockreich** Minecraft-Server!

## Funktionen

* Bankfunktionen über ein Chatmenü an dafür vorgesehenen Bankomaten (Kein *"Telebanking"*).
* User müssen sich auf die Bankomaten nur zu bewegen um den Bankomaten zu Aktivieren
* Übersichtliches, klickbares Chatmenü
* Auszahlen in Items und einzahlen aus Items. (Zusammenfassbare Items werden unterstützt)
* Überweisungen zwischen Spieler

## Befehle
`/addbankomat` - Fügt einen Bankomaten hinzu.  
`/listbankomats` - listet die Bankomaten auf.  
`/saveandreload` - Debugbefehl, speichert die Daten und ladet sie neu.  

### nicht sichtbare Befehle (nur nach Aktivieren eines Bankomaten)
`/auszahlen [Amount]` - Zahlt `[Amount]` an den Spieler aus, **wenn er noch genügend Geld auf dem Konto hat**  
`/einzahlen [Limit]` - Zahlt maximal `[Limit]` auf das Konto des Users ein.  
`/transaktion [Player] [Amount]` - Überweißt `[Amount]` an `[Player]`  **falls er noch genügend Geld auf dem Konto hat**  